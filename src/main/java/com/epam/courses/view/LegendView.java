package com.epam.courses.view;

import com.epam.courses.controller.CharacterController;
import com.epam.courses.controller.GameController;
import com.epam.courses.controller.LevelController;
import com.epam.courses.model.enums.ArtifactType;
import com.epam.courses.model.enums.Direction;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import static com.epam.courses.view.ConsoleColors.*;

public class LegendView {
    private static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    private static final String SYMBOL = PURPLE_BACKGROUND + " " + RESET;

    private LevelController levelController = new LevelController();
    private CharacterController characterController = new CharacterController();
    private GameController gameController = new GameController();

    public String showTypeDirectionToString() {
        Direction[] values = Direction.values();
        String line = "|\t" + PURPLE_UNDERLINED + "ENTER A LETTER TO CONTINUE THE GAME:" + BLUE_BOLD + " [E D S F]: ";
        line += "\n";
        line += "|\t\n|\t\t\t\t";
        for (int i = 0; i < values.length; i++) {
            line += RED + values[i] + " -> " + BLUE_BOLD + values[i].getLetterToMove() + "\t\t" + RESET;
        }
        line += "\n|_________________________________________________________________________";
        return line;
    }

    private String showArtifactTypeToString() {
        ArtifactType[] values = ArtifactType.values();
        String line = "|\t" + PURPLE_UNDERLINED + "ARTIFACT TYPES:" + RESET + " ";
        for (ArtifactType value : values)
            line += BLUE_BOLD + value + "\t" + RESET;
        line += "\n";
        line += "|\t\n";
        return line;
    }

    private String showLevelName() {
        return "\n" + PURPLE_UNDERLINED + "LEVEL" + BLUE_BOLD + "\t" + levelController.getLevel() + RESET + "";
    }

    public String showLegendPanelToString() {
        String line = "\n\n\n" + "_________________________________________________________________________\n|\n" +
                "|\t" + PURPLE_UNDERLINED + "LEVEL" + BLUE_BOLD + "\t" + levelController.getLevel() + RESET + "\n" + "|\t" + "\n";
        line += "|\t" + PURPLE_UNDERLINED + "HERO" + BLUE_BOLD + "\t" + characterController.getName() + RESET + "\n" + "|\t" + "\n";
        line += showArtifactTypeToString();
        line += "|\t" + PURPLE_UNDERLINED + "ENTER A LETTER TO QUIT THE GAME:" + BLUE_BOLD + "\tQ" + RESET + "\n|\t\n";
        line += showTypeDirectionToString();
//        line += move();
        return line;
    }

    public String showLegendPanelToString1() {
        String line = "";
        line += addLineOfFrame();
        line += String.format("%s%68s%s", SYMBOL, showLevelName(), SYMBOL);
        line += String.format("%s%68s\n", SYMBOL, SYMBOL);
        line += addLineOfFrame();
        return line;
    }

    public static String addLineOfFrame() {
        String line = "";
        for (int i = 0; i < 60; i++) {
            line += SYMBOL;
        }
        return line;
    }


}
