package com.epam.courses.view;

import com.epam.courses.controller.CharacterController;
import com.epam.courses.controller.GameController;
import com.epam.courses.controller.LevelController;
import com.epam.courses.model.enums.Direction;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Game {

    private static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

    private CharacterController characterController;
    private GameController gameController;
    private LevelController levelController;
    private CurrentSituationView currentSituationView;
    private LegendView legendView;
    private MapView mapView;
    private IntroductionView introductionView;

    public Game() {
        characterController = new CharacterController();
        gameController = new GameController();
        levelController = new LevelController();
        currentSituationView = new CurrentSituationView();
        legendView = new LegendView();
        mapView = new MapView();
        introductionView = new IntroductionView();
    }


    public String move() {
        boolean continueLoop = true;
        String line = "";
        System.out.print(ConsoleColors.BLUE + "Input direction: ");
        do {
            try {
                line = br.readLine();
                continueLoop = false;
            } catch (IOException e) {
                e.printStackTrace();
            }
        } while (continueLoop);
        return line;
    }

    public void start() {
        introductionView.introduceMenu();
        System.out.println("Š");
        String param = "";
        try {
            introductionView.playSound("game.wav");
        } catch (Exception e) {
            e.printStackTrace();
        }
        new Game().renderView();
        do {
            switch (param) {
                case "E":
                    characterController.moveCharacter(Direction.UP);
                    System.out.println("Š");
                    new Game().renderView();
                    param = move().toUpperCase();
                    break;
                case "D":
                    characterController.moveCharacter(Direction.DOWN);
                    System.out.println("Š");
                    new Game().renderView();
                    param = move().toUpperCase();
                    break;
                case "S":
                    characterController.moveCharacter(Direction.LEFT);
                    System.out.println("Š");
                    new Game().renderView();
                    param = move().toUpperCase();
                    break;
                case "F":
                    characterController.moveCharacter(Direction.RIGHT);
                    System.out.println("Š");
                    new Game().renderView();
                    param = move().toUpperCase();
                    break;
                default:
                    param = move().toUpperCase();
            }
        } while (!"Q".equals(param));
    }

    public String renderView() {
        String s1 = mapView.converteMassive1();
        String s2 = legendView.showLegendPanelToString();
        String s3 = currentSituationView.gameState();
        String[] mapArray = s1.split("\n");
        String[] legendArray = s2.split("\n");
        String[] currentSituationArray = s3.split("\n");
        String[] finalMap = new String[mapArray.length];
        int counter = 0;
        int subtract = 4;
        for (int i = 0; i < legendArray.length; i++) {
            finalMap[i] = mapArray[i].concat("\t\t\t").concat(legendArray[i]);
            System.out.println(finalMap[i]);
            counter = i;
        }
        for (int i = counter + 1; i < legendArray.length + subtract; i++) {
            finalMap[i] = mapArray[i];
            System.out.println(finalMap[i]);
            counter = i;
        }
        for (int i = 0; i < currentSituationArray.length; i++) {
            counter++;
            finalMap[counter] = mapArray[counter].concat("\t\t\t").concat(currentSituationArray[i]);
            System.out.println(finalMap[counter]);
        }
        for (int i = counter + 1; i < mapArray.length; i++) {
            counter++;
            System.out.println(mapArray[counter]);
        }
        return finalMap.toString();
    }

    public void addFrameForLegend() {

    }
}
