package com.epam.courses.view;

import sun.audio.AudioPlayer;
import sun.audio.AudioStream;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Scanner;

import static com.epam.courses.view.ConsoleColors.*;

public class IntroductionView {
    public void introduceMenu() {
        String intro =
                GREEN + "                  " + RED + " ,  /\\  ." + GREEN + "                 \n" +
                        GREEN + "                 " + RED + " //`-||-'\\\\   " + GREEN + "             \n" +
                        GREEN + " _______ _       " + RED + "(| -=||=- |)" + GREEN + " _                               _              \n" +
                        GREEN + "|__   __| |      " + RED + " \\\\,-||-.//" + GREEN + " | |                             | |\n" +
                        GREEN + "   | |  | |__   ___  " + BLACK + " ||     " + GREEN + "| |     ___  __ _  ___ _ __   __| |\n" +
                        GREEN + "   | |  | '_ \\ / _ \\" + BLACK + "  ||   " + GREEN + "  | |    / _ \\/ _` |/ _ \\ '_ \\ / _` | \n" +
                        GREEN + "   | |  | | | |  __/  " + BLACK + "||     " + GREEN + "| |___|  __/ (_| |  __/ | | | (_| |\n" +
                        GREEN + "   |_|  |_| |_|\\___| " + BLACK + " ||    " + GREEN + " |______\\___|\\__, |\\___|_| |_|\\__,_|\n" +
                        GREEN + "                      " + BLACK + "||     " + GREEN + "             __/ |                   \n" +
                        GREEN + "                      " + RED + "()     " + GREEN + "            |___/                   \n";


        String[] array = intro.split("\n");
        try {
            new IntroductionView().playSound("1.wav");
        } catch (Exception e) {
            e.printStackTrace();
        }
        for (int i = 0; i < 10; i++) {
            System.out.println(array[i]);
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    public void playSound(String fileName) throws Exception {
        InputStream in = new FileInputStream(fileName);
        AudioStream audioStream = new AudioStream(in);
        AudioPlayer.player.start(audioStream);
    }

    public void showEntryMenu() {
        String entryMenu =
                "   START GAME    \n" +
                        "    ABOUT    \n" +
                        ">    QUIT    <\n";
        String[] entyMenuMassive = entryMenu.split("\n");
        Scanner scanner = new Scanner(System.in);
        String command;
        for (int j = 0; j < entyMenuMassive.length; j++) {
            System.out.println(entyMenuMassive[j]);


            while (true) {
                command = scanner.nextLine().toUpperCase();
                switch (command) {
                    case "Z":
                        for (int i = 0; i < entyMenuMassive.length; i++) {
                            String mc = Character.toString(entyMenuMassive[i].charAt(0));
                            if (mc.endsWith(">")) {
                                entyMenuMassive[i - 1] = ">" + entyMenuMassive[i - 1] + "<";
                                entyMenuMassive[i] = entryMenu.replace("> ", "");
                                System.out.println(entyMenuMassive[i]);
                                entyMenuMassive[i] = entryMenu.replace("<", "");

                            }
                        }
                        for (int k = 0; k < entyMenuMassive.length; k++) {
                            System.out.println(entyMenuMassive[k]);
                        }

                        break;
                    case "X":
                        for (int i = 0; i < entyMenuMassive.length; i++) {
                            String mc = Character.toString(entyMenuMassive[i].charAt(0));
                            System.out.println(mc);
                            if (mc == ">") {
                                entyMenuMassive[i + 1] = ">" + entyMenuMassive[i + 1] + "<";
                                entyMenuMassive[i] = entryMenu.replace("> ", "");
                                entyMenuMassive[i] = entryMenu.replace("<", "");
                            }
                        }
                        for (int m = 0; j < entyMenuMassive.length; j++) {
                            System.out.println(entyMenuMassive[j]);
                        }
                        break;
                    default:
                        if (command == "") {

                        }
                }
            }


        }
    }

}
