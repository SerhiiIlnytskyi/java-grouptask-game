package com.epam.courses.view;

import com.epam.courses.controller.LevelController;

import static com.epam.courses.view.ConsoleColors.*;

public class MapView {

  LevelController levelController = new LevelController();
  public static final int cellSize = 3;
  public static String wall =
          "▨▨▨▨\n" +
          "▨▨▨▨\n" +
          "▨▨▨▨\n";
  public static String empty =
          "☷☷☷☷\n" +
          "☷☷☷☷\n" +
          "☷☷☷☷\n";
  public static String hero =
          "  ︿  \n" +
          " 〈〘〙〉\n" +
          "  ﹀  \n";
  public static String monster =
          " ↖  ↗ \n" +
          " 〘\uD83D\uDE08..\n" +
          " ↙  ↘ \n";
  public static String artifact = BLUE +
          " |---\n" +
          " |   |\n" +
          " \\_//\n";

    public String converteMassive1() {
        int[][] mapData = levelController.getCurrentMapData();

        StringBuilder mapRow = new StringBuilder();

        for (int x = 0; x < mapData.length; x++) {
            StringBuilder[] lines = new StringBuilder[cellSize];
            for (int i = 0; i < lines.length; i++) {
                lines[i] = new StringBuilder("");
            }
            for (int y = 0; y < mapData.length; y++) {
                switch (mapData[x][y]) {
                    case 0:
                        for (int i = 0; i < lines.length; i++) {
                            lines[i].append(RED + wall.split("\n")[i]);
                        }
                        break;
                    case 1:
                        for (int i = 0; i < lines.length; i++) {
                            lines[i].append(WHITE_BACKGROUND + empty.split("\n")[i]);
                        }
                        break;
                    case 2:
                        for (int i = 0; i < lines.length; i++) {
                            lines[i].append(BLUE_BACKGROUND + hero.split("\n")[i]);
                        }
                        break;
                    case 3:
                        for (int i = 0; i < lines.length; i++) {
                            lines[i].append(CYAN_BACKGROUND + monster.split("\n")[i]);
                        }
                        break;
                    case 4:
                        for (int i = 0; i < lines.length; i++) {
                            lines[i].append(BLACK + artifact.split("\n")[i]);
                        }
                        break;
                    default:
                        break;
                }
            }
            for (int i = 0; i < lines.length; i++) {
                mapRow.append(lines[i]).append("\n");
            }
        }
        return mapRow.toString();
    }
}



