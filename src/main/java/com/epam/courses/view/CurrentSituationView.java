package com.epam.courses.view;

import com.epam.courses.controller.BattleController;
import com.epam.courses.controller.CharacterController;
import com.epam.courses.controller.GameController;
import com.epam.courses.model.enums.GameState;

import static com.epam.courses.view.ConsoleColors.*;
import static com.epam.courses.view.ConsoleColors.BLUE_BOLD;

public class CurrentSituationView {
    private CharacterController characterController = new CharacterController();
    private GameController gameController = new GameController();
    private BattleController battleController = new BattleController();

    private String heroName = "_________________________________________________________________________\n|\n|\t" + PURPLE_UNDERLINED + "HERO" + BLUE_BOLD + "\t" + characterController.getName() + RESET + "\n|\n";
    private String monsterName = "|\t" + PURPLE_UNDERLINED + "MONSTER" + BLUE_BOLD + "\t" + battleController.getMonsterName() + RESET + "\n|\n";
    private String currentAxis = "|\t" + PURPLE_UNDERLINED + "THE CURRENT COORDINATES OF THE HERO [X Y]:" + BLUE_BOLD + " " +
            characterController.getCharacterCurrentXAxis() + "\t" + characterController.getCharacterCurrentYAxis() + RESET + "\n|\n";
    private String getCurrentHealth = "|\t" + PURPLE_UNDERLINED + "THE HERO'S CURRENT/MAX HEALTH:" + BLUE_BOLD + "\t" + " " +
            characterController.getCharacterCurrentHealth() + "/" + characterController.getCharacterMaxHealth() + RESET + "\n|\n";
    private String getStrength = "|\t" + PURPLE_UNDERLINED + "THE HERO'S STRENGTH:" + BLUE_BOLD + " " + "\t" +
            characterController.getCharacterStrength() + RESET + "\n|\n";
    private String getDefence = "|\t" + PURPLE_UNDERLINED + "THE HERO'S DEFENCE:" + BLUE_BOLD + " " + "\t" +
            characterController.getCharacterDefence() + RESET + "\n|\n";
    private String getMessageIfAWall = "|\t" + PURPLE_UNDERLINED + "THE HERO CANNOT MOVE BECAUSE THE" + BLUE_BOLD + "\t" +
            characterController.getMessageIfAWall() + RESET + "\n|\n";
    private String getMessageIfHeroDied = "|\t" + PURPLE_UNDERLINED + "THE HERO IS DIED" + RESET + "\n|\n";
    private String getMessageIfHeroTakeArtifact = "|\t" + PURPLE_UNDERLINED + "THE HERO TAKE AN ARTIFACT" + BLUE_BOLD + "\t" +
            battleController.getArtifactType() + "\t" + battleController.getArtifactStrength() + RESET + "\n|\n";
    private String getMessageIfMonsterDied = "|\t" + RED_BOLD + characterController.getMessageIfMonsterDied() + RESET + "\n|\n";
    private String getMessageIfBattle = "|\t" + PURPLE_UNDERLINED + battleController.battleMove() + RESET +
            "\t" + BLUE_BOLD + characterController.getMessageIfBattle() + RESET + "\n|\n";
    private String getMonsterStrength = "|\t" + PURPLE_UNDERLINED + "THE MONSTER'S STRENGTH:" + BLUE_BOLD +
            "\t" + battleController.getMonsterStrength() + "\n|\n" +
            "|\t" + PURPLE_UNDERLINED + "THE MONSTER'S CURRENT/MAX HEALTH" + BLUE_BOLD + "\t" +
            battleController.getMonsterCurrentHealth() + "/" + battleController.getMonsterMaxHealth() +
            RESET + "\n|\n" +"|\t" + PURPLE_UNDERLINED + "THE MONSTER DEFENSE:" + BLUE_BOLD + "\t" +
            battleController.getMonsterDefense() + RESET + "\n|\n" +
            "_________________________________________________________________________";


    private String showStandardPartCurrentSituationPanel() {
        return heroName + currentAxis + getCurrentHealth + getStrength + getDefence;
    }

    private String showPanelIfHeroNotToMove() {
        return getMessageIfAWall;
    }

    private String showPanelIfBattleStart() {
        return monsterName + getMessageIfBattle + getMonsterStrength;
    }

    private String showPanelIfHeroDied() {
        return getMessageIfHeroDied;
    }

    private String showPanelIfMonsterDied() {
        return getMessageIfMonsterDied;
    }

    private String showPanelIfHeroTakeArtifact() {
        return getMessageIfHeroTakeArtifact;
    }

    private String showBattleInfo() {
        return getMonsterStrength;
    }

    public String gameState() {
//        System.out.println(showTypeGameState());
        String infoToPanelGame = "";

        GameState gameState = gameController.getGameState();
        switch (gameState) {
            case HERO_MOVE:
                infoToPanelGame = showStandardPartCurrentSituationPanel();
//                System.out.println(showStandardPartCurrentSituationPanel());
                break;
            case HERO_CANT_MOVE:
                infoToPanelGame = showStandardPartCurrentSituationPanel() +
                        showPanelIfHeroNotToMove();
//                System.out.println(showStandardPartCurrentSituationPanel());
//                System.out.println(showPanelIfHeroNotToMove());
                break;
            case BATTLE_MOVE:
                infoToPanelGame = showStandardPartCurrentSituationPanel()
                        + showPanelIfBattleStart();
//                System.out.println(showStandardPartCurrentSituationPanel());
//                System.out.println(showPanelIfBattleStart());
                break;
            case TAKE_ARTIFACT:
                infoToPanelGame = showStandardPartCurrentSituationPanel() +
                        showPanelIfHeroTakeArtifact();
//                System.out.println(showStandardPartCurrentSituationPanel());
//                System.out.println(showPanelIfHeroTakeArtifact());
                break;
            case MONSTER_DIED:
                infoToPanelGame = showStandardPartCurrentSituationPanel() +
                        showPanelIfMonsterDied();
//                System.out.println(showStandardPartCurrentSituationPanel());
//                System.out.println(showPanelIfMonsterDied());
                break;
            case HERO_DIED:
                infoToPanelGame = showStandardPartCurrentSituationPanel() +
                        showPanelIfHeroDied();
//                System.out.println(showStandardPartCurrentSituationPanel());
//                System.out.println(showPanelIfHeroDied());
                break;
        }
        return infoToPanelGame;
    }

    public String showTypeGameState() {
        GameState[] values = GameState.values();
        String line;
        line = PURPLE_UNDERLINED + "Show game state:" + PURPLE_BOLD + "\n";
        line += "\t\t\t";
        for (int i = 0; i < values.length; i++) {
            line += BLUE_BOLD + "#" + i + " " + values[i] + "\t\t" + RESET;
        }
        return line;
    }
}
