package com.epam.courses.model;

public class Character {

    private String name;
    private Integer heroLevel;
    private Integer strength;
    private Integer healthPoints;
    private Integer currentHealthPoints;
    private Integer defence;
    private Integer coordinateXAxis;
    private Integer coordinateYAxis;
    private Equipment equipment;

    public Character(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getHeroLevel() {
        return heroLevel;
    }

    public void setHeroLevel(Integer heroLevel) {
        this.heroLevel = heroLevel;
    }

    public Integer getStrength() {
        return strength;
    }

    public void setStrength(Integer strength) {
        this.strength = strength;
    }

    public Integer getHealthPoints() {
        return healthPoints;
    }

    public void setHealthPoints(Integer healthPoints) {
        this.healthPoints = healthPoints;
    }

    public Integer getMaximumHealthPoints() {
        return currentHealthPoints;
    }

    public void setCurrentHealthPoints(Integer currentHealthPoints) {
        this.currentHealthPoints = currentHealthPoints;
    }

    public Equipment getEquipment() {
        return equipment;
    }

    public void setEquipment(Equipment equipment) {
        this.equipment = equipment;
    }

    public Integer getDefence() {
        return defence;
    }

    public void setDefence(Integer defence) {
        this.defence = defence;
    }

    public Integer getCoordinateXAxis() {
        return coordinateXAxis;
    }

    public void setCoordinateXAxis(Integer coordinateXAxis) {
        this.coordinateXAxis = coordinateXAxis;
    }

    public Integer getCoordinateYAxis() {
        return coordinateYAxis;
    }

    public void setCoordinateYAxis(Integer coordinateYAxis) {
        this.coordinateYAxis = coordinateYAxis;
    }
}
