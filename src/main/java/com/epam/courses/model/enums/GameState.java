package com.epam.courses.model.enums;

public enum GameState {
    HERO_MOVE, HERO_CANT_MOVE, BATTLE_MOVE, MONSTER_DIED, HERO_DIED, TAKE_ARTIFACT
}
