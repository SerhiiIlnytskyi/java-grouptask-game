package com.epam.courses.model.enums;

public enum Direction {
    UP("E"), DOWN("D"), LEFT("S"), RIGHT("F");

    private final String letterToMove;

    Direction(String letterToMove) {
        this.letterToMove = letterToMove;
    }

    public String getLetterToMove() {
        return letterToMove;
    }
}
