package com.epam.courses.model.enums;

public enum ArtifactType {
    ARMOUR, GLOVES, BOOTS, HELMET, PANTS, SWORD, SHIELD;
}
