package com.epam.courses.model;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Map {

    private File mapFile;
    private int[][] mapData;
    private List<Monster> monsters = new ArrayList<Monster>();
    private int mapLevel;

    public Map(int[][] mapData) {
        this.mapData = mapData;
    }

    public File getMapFile() {
        return mapFile;
    }

    public void setMapFile(File mapFile) {
        this.mapFile = mapFile;
    }

    public int[][] getMapData() {
        return mapData;
    }

    public void setMapData(int[][] mapData) {
        this.mapData = mapData;
    }

    public List<Monster> getMonsters() {
        return monsters;
    }

    public void setMonsters(List<Monster> monsters) {
        this.monsters = monsters;
    }

    public int getMapLevel() {
        return mapLevel;
    }

    public void setMapLevel(int mapLevel) {
        this.mapLevel = mapLevel;
    }

    public void addMonster(Monster monster) {
        this.monsters.add(monster);
    }

    public void removeMonster(Monster currentBattleMonster) {
        this.getMonsters().remove(currentBattleMonster);
        this.mapData[currentBattleMonster.getCoordinateXAxis()][currentBattleMonster
                .getCoordinateYAxis()] = 4;
    }
}
