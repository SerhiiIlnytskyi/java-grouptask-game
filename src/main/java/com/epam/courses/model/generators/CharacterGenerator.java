package com.epam.courses.model.generators;

import com.epam.courses.model.Character;
import com.epam.courses.model.Equipment;

public class CharacterGenerator {
    private static final int BASIC_HERO_LEVEL = 0;
    private static final int BASIC_HERO_STRENGTH = 10;
    private static final int BASIC_HERO_HEALTH_POINTS = 100;
    private static final int BASIC_HERO_DEFENCE = 10;

    public Character generatePlayer(String name) {
        Character character = new Character(name);
        character.setCoordinateXAxis(1);
        character.setCoordinateYAxis(1);
        character.setEquipment(new Equipment());
        character.setDefence(BASIC_HERO_DEFENCE);
        character.setHealthPoints(BASIC_HERO_HEALTH_POINTS);
        character.setCurrentHealthPoints(BASIC_HERO_HEALTH_POINTS);
        character.setStrength(BASIC_HERO_STRENGTH);
        character.setHeroLevel(BASIC_HERO_LEVEL);
        return character;
    }

}
