package com.epam.courses.model.generators;

import com.epam.courses.model.Monster;

public class MonsterGenerator {

    private static final int BASIC_MONSTER_LEVEL = 1;
    private static final int BASIC_MONSTER_STRENGTH = 5;
    private static final int BASIC_MONSTER_HEALTH_POINTS = 30;
    private static final int BASIC_MONSTER_DEFENCE = 5;

    public Monster generateMonster(String name, Integer coordinateXAxis, Integer coordinateYAxis,
                                   Integer levelFactor) {
        Monster monster = new Monster(name, coordinateXAxis, coordinateYAxis);
        monster.setMonsterLevel(BASIC_MONSTER_LEVEL * levelFactor);
        monster.setStrength(BASIC_MONSTER_STRENGTH * levelFactor);
        monster.setHealthPoints(BASIC_MONSTER_HEALTH_POINTS * levelFactor);
        monster.setCurrentHealthPoints(BASIC_MONSTER_HEALTH_POINTS * levelFactor);
        monster.setDefence(BASIC_MONSTER_DEFENCE * levelFactor);
        return monster;
    }

    public Monster generateMonster() {
        return generateMonster("None", 1, 1, 1);
    }
}
