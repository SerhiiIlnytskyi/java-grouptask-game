package com.epam.courses.model.generators;

import com.epam.courses.model.Artifact;
import com.epam.courses.model.enums.ArtifactType;

public class ArtifactGenerator {

    private int levelMapFactor;

    public Artifact getRandomArtifact() {
        Artifact artifact = new Artifact();
        artifact.setArtifactType(getRandomArtifactType());
        artifact = generatePointsByArtifact(artifact);
        return artifact;
    }

    private int randomize(int minStrength, int maxStrength) {
        return (int) (Math.random() * ((maxStrength - minStrength) + 1)) + minStrength;
    }

    private ArtifactType getRandomArtifactType() {
        return ArtifactType.values()[(int) (Math.random() * ArtifactType.values().length)];
    }

    private int getLevelFactor() {
        int levelFactor;
        switch (levelMapFactor) {
            case 1:
                levelFactor = 1;
            case 2:
                levelFactor = 2;
            case 3:
                levelFactor = 3;
            case 4:
                levelFactor = 4;
            case 5:
                levelFactor = 5;
            default:
                levelFactor = 0;
        }
        return levelFactor;
    }

    private Artifact generatePointsByArtifact(Artifact artifact) {
        switch (artifact.getArtifactType()) {
            case ARMOUR:
                artifact.setStrength(randomize(6, 20));
                artifact.setHealthPoints(randomize(6, 20));
                artifact.setDefence(randomize(6, 20));
                break;
            case GLOVES:
                artifact.setStrength(randomize(2, 10));
                artifact.setHealthPoints(randomize(6, 15));
                artifact.setDefence(randomize(6, 10));
                break;
            case BOOTS:
                artifact.setStrength(randomize(6, 14));
                artifact.setHealthPoints(randomize(6, 20));
                artifact.setDefence(randomize(6, 20));
                break;
            case HELMET:
                artifact.setStrength(randomize(6, 20));
                artifact.setHealthPoints(randomize(2, 20));
                artifact.setDefence(randomize(6, 20));
                break;
            case PANTS:
                artifact.setStrength(randomize(6, 20));
                artifact.setHealthPoints(randomize(3, 20));
                artifact.setDefence(randomize(6, 20));
                break;
            case SWORD:
                artifact.setStrength(randomize(6, 20));
                artifact.setHealthPoints(randomize(0, 0));
                artifact.setDefence(randomize(0, 0));
                break;
            case SHIELD:
                artifact.setStrength(randomize(4, 20));
                artifact.setHealthPoints(randomize(0, 0));
                artifact.setDefence(randomize(0, 0));
                break;
            default:
                artifact.setStrength(randomize(0, 0));
                artifact.setHealthPoints(randomize(0, 0));
                artifact.setDefence(randomize(0, 0));
        }
        return artifact;
    }
}



