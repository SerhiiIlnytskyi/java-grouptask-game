package com.epam.courses.model.logic;

import com.epam.courses.model.Map;
import com.epam.courses.model.enums.GameState;
import com.epam.courses.model.generators.MapGenerator;
import com.epam.courses.model.manager.MapManager;

public class MapLogic {

    private static MapLogic mapLogic = new MapLogic();
    static Map currentMap;
    static MapManager mapManager;

    private MapLogic() {
        currentMap = MapGenerator.map;
        mapManager = new MapManager(currentMap);
        mapManager.fillMonsters(currentMap);
    }

    public void checkCell(int xAxis, int yAxis) {
        MapManager mapManager = new MapManager(currentMap);
        if (!mapManager.isCellInMapRange(xAxis, yAxis)) {
            GameLogic.gameState = GameState.HERO_CANT_MOVE;
        } else if (mapManager.isCellWithWall(xAxis, yAxis)) {
            GameLogic.gameState = GameState.HERO_CANT_MOVE;
        } else if (mapManager.isCellWithMonster(xAxis, yAxis)) {
            GameLogic.gameState = GameState.BATTLE_MOVE;
        } else if (mapManager.isCellWithArtifact(xAxis, yAxis)) {
            GameLogic.gameState = GameState.TAKE_ARTIFACT;
        } else {
            GameLogic.gameState = GameState.HERO_MOVE;
        }
    }

    public int getCurrentMapLevel() {
        return currentMap.getMapLevel();
    }

    public int getNumberOfMonsters() {
        return currentMap.getMonsters().size();
    }

    public int[][] getCurrentMapData() {
        return currentMap.getMapData();
    }

    public static MapLogic getInstance() {
        return mapLogic;
    }

    public void setCurrentMapData(int[][] mapData) {
        currentMap.setMapData(mapData);
    }
}
