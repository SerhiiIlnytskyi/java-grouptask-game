package com.epam.courses.model.logic;

import com.epam.courses.model.Character;
import com.epam.courses.model.enums.Direction;
import com.epam.courses.model.generators.ArtifactGenerator;
import com.epam.courses.model.generators.CharacterGenerator;
import com.epam.courses.model.manager.EquipmentManager;

public class CharacterLogic {

    private static CharacterLogic characterLogic = new CharacterLogic();
    static Character character;
    BattleLogic battleLogic = BattleLogic.getInstance();

    private CharacterLogic() {
        character = new CharacterGenerator().generatePlayer("HERO");
    }

    public void moveCharacter(Direction direction) {
        int characterCurrentXAxis = character.getCoordinateXAxis();
        int characterCurrentYAxis = character.getCoordinateYAxis();

        switch (direction) {
            case UP:
                MapLogic.getInstance()
                        .checkCell(characterCurrentXAxis - 1, characterCurrentYAxis);
                perfomAction(characterCurrentXAxis - 1, characterCurrentYAxis);
                break;
            case DOWN:
                MapLogic.getInstance()
                        .checkCell(characterCurrentXAxis + 1, characterCurrentYAxis);
                perfomAction(characterCurrentXAxis + 1, characterCurrentYAxis);
                break;
            case LEFT:
                MapLogic.getInstance()
                        .checkCell(characterCurrentXAxis, characterCurrentYAxis - 1);
                perfomAction(characterCurrentXAxis, characterCurrentYAxis - 1);
                break;
            case RIGHT:
                MapLogic.getInstance()
                        .checkCell(characterCurrentXAxis, characterCurrentYAxis + 1);
                perfomAction(characterCurrentXAxis, characterCurrentYAxis + 1);
                break;
            default:
                break;
        }
    }

    public int getCharacterCurrentHealth() {
        return character.getMaximumHealthPoints();
    }

    public int getCharacterHealth() {
        return character.getHealthPoints();
    }

    public int getCharacterStrength() {
        return character.getStrength() + new EquipmentManager(character.getEquipment()).getTotalStrength();
    }

    public int getCharacterDefence() {
        return character.getDefence() + new EquipmentManager(character.getEquipment()).getTotalDefence();
    }

    public int getCharacterCurrentXAxis() {
        return character.getCoordinateXAxis();
    }

    public int getCharacterCurrentYAxis() {
        return character.getCoordinateYAxis();
    }


    private void perfomAction(int characterNextXAxis, int characterNextYAxis) {
        switch (GameLogic.gameState) {
            case HERO_MOVE:
                makeTurnToFreeCell(characterNextXAxis, characterNextYAxis);
                break;
            case BATTLE_MOVE:
                battleLogic.startBattle(characterNextXAxis, characterNextYAxis);
                break;
            case MONSTER_DIED:
                int[][] mapData = MapLogic.getInstance().getCurrentMapData();
                mapData[characterNextXAxis][characterNextYAxis] = 4;
                MapLogic.getInstance().setCurrentMapData(mapData);
                break;
            case TAKE_ARTIFACT:
                takeArtifact();
                makeTurnToFreeCell(characterNextXAxis, characterNextYAxis);
                break;
            default:

        }
    }

    private void takeArtifact() {
        EquipmentManager equipmentManager = new EquipmentManager(character.getEquipment());
        equipmentManager.equipArtifact(new ArtifactGenerator().getRandomArtifact());
    }

    public void makeTurnToFreeCell(int characterCurrentXAxis, int characterCurrentYAxis) {
        int[][] mapData = MapLogic.currentMap.getMapData();
        mapData[character.getCoordinateXAxis()][character.getCoordinateYAxis()] = 1;
        mapData[characterCurrentXAxis][characterCurrentYAxis] = 2;
        character.setCoordinateXAxis(characterCurrentXAxis);
        character.setCoordinateYAxis(characterCurrentYAxis);
        MapLogic.currentMap.setMapData(mapData);
        if (character.getHealthPoints() < character.getMaximumHealthPoints()) {
            character.setHealthPoints(getCharacterHealth() + 1);
        }
    }

    public String getCharacterName() {
        return character.getName();
    }

    public static CharacterLogic getInstance() {
        return characterLogic;
    }
}

