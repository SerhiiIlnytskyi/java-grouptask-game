package com.epam.courses.model.logic;

import com.epam.courses.model.enums.GameState;

public class GameLogic {

    private static GameLogic gameLogic = new GameLogic();
    static GameState gameState;

    private GameLogic() {
        gameState = GameState.HERO_CANT_MOVE;
    }

    public GameState getGameState() {
        return gameState;
    }

    public void setGameState(GameState gameState) {
        GameLogic.gameState = gameState;
    }

    public boolean endGame() {
        return true;
    }

    public void initGame() {
    }

    public static GameLogic getInstance() {
        return gameLogic;
    }
}

