package com.epam.courses.model.logic;

import com.epam.courses.model.Artifact;
import com.epam.courses.model.Character;
import com.epam.courses.model.Monster;
import com.epam.courses.model.enums.GameState;
import com.epam.courses.model.generators.MonsterGenerator;
import com.epam.courses.model.manager.CharacterManager;
import com.epam.courses.model.manager.MapManager;
import com.epam.courses.model.manager.MonsterManager;

public class BattleLogic {

    private static BattleLogic battleLogic = new BattleLogic();
    static Monster currentBattleMonster = new MonsterGenerator().generateMonster();
    static CharacterLogic characterLogic;
    static MapManager mapManager;
    private int lastHeroDemage;
    private int lastMonsterDemage;

    private BattleLogic() {
        mapManager = new MapManager(MapLogic.currentMap);
        characterLogic = CharacterLogic.getInstance();
    }

    public static BattleLogic getInstance() {
        return battleLogic;
    }

    public void startBattle(int xAxis, int yAxis) {
        currentBattleMonster = mapManager.getMonsterFromCell(xAxis, yAxis);
        makeBattleAction(currentBattleMonster, CharacterLogic.character);
        checkBattleResult(xAxis, yAxis);

    }

    public void checkBattleResult(int xAxis, int yAxis) {
        MonsterManager monsterManager = new MonsterManager(currentBattleMonster);
        CharacterManager characterManager = new CharacterManager(CharacterLogic.character);
        if (!monsterManager.isMonsterAlive()) {
            GameLogic.gameState = GameState.MONSTER_DIED;
            MapLogic.currentMap.getMonsters().remove(currentBattleMonster);
            MapLogic.currentMap.removeMonster(currentBattleMonster);
            winAward();
        }
        if (!characterManager.isCharacterAlive()) {
            GameLogic.gameState = GameState.HERO_DIED;
            GameLogic.getInstance().endGame();
        }
    }

    public void makeBattleAction(Monster monster, Character character) {
        MonsterManager monsterManager = new MonsterManager(monster);
        CharacterManager characterManager = new CharacterManager(character);
        lastHeroDemage = characterManager.getBlow(monsterManager.strikeBlow());
        lastMonsterDemage = monsterManager.getBlow(characterManager.strikeBlow());
        GameLogic.gameState = GameState.BATTLE_MOVE;
    }

    public Artifact winAward() {
        return new Artifact();
    }

    public Monster getCurrentBattleMonster() {
        return currentBattleMonster;
    }

    public void setCurrentBattleMonster(Monster currentBattleMonster) {
        BattleLogic.currentBattleMonster = currentBattleMonster;
    }

    public int getLastHeroDemage() {
        return lastHeroDemage;
    }

    public int getLastMonsterDemage() {
        return lastMonsterDemage;
    }
}
