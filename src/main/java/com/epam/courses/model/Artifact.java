package com.epam.courses.model;

import com.epam.courses.model.enums.ArtifactType;

public class Artifact {

    private ArtifactType artifactType;
    private int strength;
    private int healthPoints;
    private int defence;

    public Artifact() {
    }

    public Artifact(ArtifactType artifactType, Integer strength, Integer healthPoints, Integer defence) {
        this.artifactType = artifactType;
        this.strength = strength;
        this.healthPoints = healthPoints;
        this.defence = defence;
    }

    public ArtifactType getArtifactType() {
        return artifactType;
    }

    public void setArtifactType(ArtifactType artifactType) {
        this.artifactType = artifactType;
    }

    public Integer getStrength() {
        return strength;
    }

    public void setStrength(Integer strength) {
        this.strength = strength;
    }

    public Integer getHealthPoints() {
        return healthPoints;
    }

    public void setHealthPoints(Integer healthPoints) {
        this.healthPoints = healthPoints;
    }

    public Integer getDefence() {
        return defence;
    }

    public void setDefence(Integer defence) {
        this.defence = defence;
    }

    @Override
    public String toString() {
        return "Artifact{" +
                "artifactType=" + artifactType +
                ", strength=" + strength +
                ", healthPoints=" + healthPoints +
                ", defence=" + defence +
                '}';
    }
}
