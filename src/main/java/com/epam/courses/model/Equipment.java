package com.epam.courses.model;

public class Equipment {
    private Artifact armour = new Artifact();
    private Artifact gloves = new Artifact();
    private Artifact boots = new Artifact();
    private Artifact helmet = new Artifact();
    private Artifact pants = new Artifact();
    private Artifact sword = new Artifact();
    private Artifact shield = new Artifact();

    public Artifact getArmour() {
        return armour;
    }

    public void setArmour(Artifact armour) {
        this.armour = armour;
    }

    public Artifact getGloves() {
        return gloves;
    }

    public void setGloves(Artifact gloves) {
        this.gloves = gloves;
    }

    public Artifact getBoots() {
        return boots;
    }

    public void setBoots(Artifact boots) {
        this.boots = boots;
    }

    public Artifact getHelmet() {
        return helmet;
    }

    public void setHelmet(Artifact helmet) {
        this.helmet = helmet;
    }

    public Artifact getPants() {
        return pants;
    }

    public void setPants(Artifact pants) {
        this.pants = pants;
    }

    public Artifact getSword() {
        return sword;
    }

    public void setSword(Artifact sword) {
        this.sword = sword;
    }

    public Artifact getShield() {
        return shield;
    }

    public void setShield(Artifact shield) {
        this.shield = shield;
    }
}
