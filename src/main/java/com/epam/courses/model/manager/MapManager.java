package com.epam.courses.model.manager;

import com.epam.courses.controller.LevelController;
import com.epam.courses.model.Map;
import com.epam.courses.model.Monster;
import com.epam.courses.model.generators.MonsterGenerator;

public class MapManager {

    private Map currentMap;

    public MapManager(Map currentMap) {
        this.currentMap = currentMap;
    }

    public boolean isCellWithMonster(int xAxis, int yAxis) {
        int[][] currentMapData = currentMap.getMapData();
        if (currentMapData[xAxis][yAxis] == 3) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isCellWithWall(int xAxis, int yAxis) {
        int[][] currentMapData = currentMap.getMapData();
        if (currentMapData[xAxis][yAxis] == 0) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isCellInMapRange(int xAxis, int yAxis) {
        int[][] currentMapData = currentMap.getMapData();
        return (xAxis < currentMapData.length) &&
                (yAxis < currentMapData[0].length) &&
                (xAxis > 0) && (yAxis > 0);
    }

    public boolean isCellEmpty(int xAxis, int yAxis) {
        int[][] currentMapData = currentMap.getMapData();
        if (currentMapData[xAxis][yAxis] == 1) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isCellWithArtifact(int xAxis, int yAxis) {
        int[][] currentMapData = currentMap.getMapData();
        if (currentMapData[xAxis][yAxis] == 4) {
            return true;
        } else {
            return false;
        }
    }

    public Monster getMonsterFromCell(int xAxis, int yAxis) {
        Monster cellMonster = null;
        for (Monster monster : currentMap.getMonsters()) {
            if ((monster.getCoordinateXAxis() == xAxis) &&
                    (monster.getCoordinateYAxis() == yAxis)) {
                cellMonster = monster;
            }
        }
        return cellMonster;
    }

    public Map fillMonsters(Map map) {
        int[][] mapData = map.getMapData();
        for (int x = 0; x < mapData.length; x++) {
            for (int y = 0; y < mapData.length; y++) {
                if (mapData[x][y] == 3) {
                    map.addMonster(new MonsterGenerator().generateMonster("BATMAN", x, y, 1));
                }
            }
        }
        return map;
    }

    public void replacementonster(int xAxia, int yAxis) {
        LevelController levelController = new LevelController();
        int mapData[][] = levelController.getCurrentMapData();
        mapData[xAxia][yAxis] = 5;
    }

}
