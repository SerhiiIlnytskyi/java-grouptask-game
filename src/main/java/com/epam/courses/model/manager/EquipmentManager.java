package com.epam.courses.model.manager;

import com.epam.courses.model.Artifact;
import com.epam.courses.model.Equipment;

public class EquipmentManager {

    private Equipment equipment;

    public EquipmentManager(Equipment equipment) {
        this.equipment = equipment;
    }

    public int getTotalHealthPoints() {
        int healthPoints = 0;
        healthPoints += equipment.getArmour().getHealthPoints();
        healthPoints += equipment.getBoots().getHealthPoints();
        healthPoints += equipment.getGloves().getHealthPoints();
        healthPoints += equipment.getPants().getHealthPoints();
        healthPoints += equipment.getShield().getHealthPoints();
        healthPoints += equipment.getSword().getHealthPoints();
        healthPoints += equipment.getHelmet().getHealthPoints();
        return healthPoints;
    }

    public int getTotalStrength() {
        int strength = 0;
        strength += equipment.getArmour().getStrength();
        strength += equipment.getBoots().getStrength();
        strength += equipment.getGloves().getStrength();
        strength += equipment.getPants().getStrength();
        strength += equipment.getShield().getStrength();
        strength += equipment.getSword().getStrength();
        strength += equipment.getHelmet().getStrength();
        return strength;
    }

    public int getTotalDefence() {
        int defence = 0;
        defence += equipment.getArmour().getDefence();
        defence += equipment.getBoots().getDefence();
        defence += equipment.getGloves().getDefence();
        defence += equipment.getPants().getDefence();
        defence += equipment.getShield().getDefence();
        defence += equipment.getSword().getDefence();
        defence += equipment.getHelmet().getDefence();
        return defence;
    }


    public void equipArtifact(Artifact artifact) {
        switch (artifact.getArtifactType()) {
            case BOOTS:
                equipment.setBoots(artifact);
                break;
            case PANTS:
                equipment.setPants(artifact);
                break;
            case SWORD:
                equipment.setSword(artifact);
                break;
            case ARMOUR:
                equipment.setArmour(artifact);
                break;
            case GLOVES:
                equipment.setGloves(artifact);
                break;
            case HELMET:
                equipment.setHelmet(artifact);
                break;
            case SHIELD:
                equipment.setShield(artifact);
                break;
            default:
                break;
        }
    }
}
