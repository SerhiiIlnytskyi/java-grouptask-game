package com.epam.courses.model.manager;

import com.epam.courses.model.Character;

public class CharacterManager {
    private Character character;
    private EquipmentManager equipmentManager;

    public CharacterManager(Character character) {
        this.character = character;
        this.equipmentManager = new EquipmentManager(character.getEquipment());
    }

    public Integer strikeBlow() {
        int attack = (int) (getAttackFactor() * character.getStrength());
        return attack;
    }

    public int getBlow(Integer healthPoints) {
        int damage = (int) (healthPoints / getDefenceFactor());
        character.setHealthPoints(character.getHealthPoints() - damage);
        return damage;
    }

    public double getDefenceFactor() {
        double defenceFactor = 1.0;
        defenceFactor += 1.0 * (character.getDefence() + equipmentManager.getTotalDefence()) / 100;
        return defenceFactor;
    }

    public double getAttackFactor() {
        double attackFactor = 1.0;
        attackFactor += (1.0 * (character.getStrength() + equipmentManager.getTotalStrength())) / 100;
        return attackFactor;
    }

    public boolean isCharacterAlive() {
        return character.getHealthPoints() > 0;
    }

    public Character getCharacter() {
        return character;
    }

    public void setCharacter(Character character) {
        this.character = character;
    }
}
