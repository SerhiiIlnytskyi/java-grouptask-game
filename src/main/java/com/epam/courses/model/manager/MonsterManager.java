package com.epam.courses.model.manager;

import com.epam.courses.model.Artifact;
import com.epam.courses.model.Monster;

public class MonsterManager {

    private Monster monster;

    public MonsterManager(Monster monster) {
        this.monster = monster;
    }

    public Integer strikeBlow() {
        int attack = (int) getAttackFactor() * monster.getStrength();
        return attack;
    }

    public int getBlow(Integer healthPoints) {
        int damage = (int) ((healthPoints * 1.0) / getDefenceFactor());
        monster.setHealthPoints(monster.getHealthPoints() - damage);
        return damage;
    }

    public double getDefenceFactor() {
        double defenceFactor = 1.0;
        defenceFactor += 1.0 * (monster.getDefence()) / 100;
        return defenceFactor;
    }

    public double getAttackFactor() {
        double attackFactor = 1.0;
        attackFactor += 1.0 * (monster.getStrength()) / 100;
        return attackFactor;
    }

    public boolean isMonsterAlive() {
        return monster.getHealthPoints() > 0;
    }

    public Artifact winAward() {
        return new Artifact();
    }
}

