package com.epam.courses.model;

public class Monster {


    private String name;
    private Integer coordinateXAxis;
    private Integer coordinateYAxis;
    private Integer monsterLevel;
    private Integer strength;
    private Integer healthPoints;
    private Integer currentHealthPoints;
    private Integer defence;

    public Monster(String name, Integer coordinateXAxis, Integer coordinateYAxis) {
        this.name = name;
        this.coordinateXAxis = coordinateXAxis;
        this.coordinateYAxis = coordinateYAxis;
    }

    public Monster() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getMonsterLevel() {
        return monsterLevel;
    }

    public void setMonsterLevel(Integer monsterLevel) {
        this.monsterLevel = monsterLevel;
    }

    public Integer getStrength() {
        return strength;
    }

    public void setStrength(Integer strength) {
        this.strength = strength;
    }

    public Integer getHealthPoints() {
        return healthPoints;
    }

    public void setHealthPoints(Integer healthPoints) {
        this.healthPoints = healthPoints;
    }

    public Integer getCurrentHealthPoints() {
        return currentHealthPoints;
    }

    public void setCurrentHealthPoints(Integer currentHealthPoints) {
        this.currentHealthPoints = currentHealthPoints;
    }

    public Integer getCoordinateXAxis() {
        return coordinateXAxis;
    }

    public void setCoordinateXAxis(Integer coordinateXAxis) {
        this.coordinateXAxis = coordinateXAxis;
    }

    public Integer getCoordinateYAxis() {
        return coordinateYAxis;
    }

    public void setCoordinateYAxis(Integer coordinateYAxis) {
        this.coordinateYAxis = coordinateYAxis;
    }

    public Integer getDefence() {
        return defence;
    }

    public void setDefence(Integer defence) {
        this.defence = defence;
    }
}
