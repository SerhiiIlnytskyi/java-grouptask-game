package com.epam.courses.controller;

import com.epam.courses.model.logic.GameLogic;
import com.epam.courses.model.enums.GameState;

public class GameController {
    private GameLogic gameLogic;

    public GameController() {
        gameLogic = GameLogic.getInstance();
    }

    public void initGame() {
        gameLogic.initGame();
    }

    public boolean exitGame() {
        return gameLogic.endGame();
    }

    public GameState getGameState() {
        return gameLogic.getGameState();
    }

}
