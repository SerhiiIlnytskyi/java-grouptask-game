package com.epam.courses.controller;


import com.epam.courses.model.enums.ArtifactType;
import com.epam.courses.model.logic.BattleLogic;


public class BattleController {

    private static BattleLogic battleLogic;

    public BattleController() {
        battleLogic = BattleLogic.getInstance();
    }

    public String battleMove() {
        return "Hero Damage = " + battleLogic.getLastHeroDemage() + " : " +
                "Monster Damage = " + battleLogic.getLastMonsterDemage();
    }

    public int getMonsterStrength() {
        return battleLogic.getCurrentBattleMonster().getStrength();
    }

    public int getMonsterCurrentHealth() {
        return battleLogic.getCurrentBattleMonster().getHealthPoints();
    }

    public int getMonsterMaxHealth() {
        return battleLogic.getCurrentBattleMonster().getCurrentHealthPoints();
    }

    public int getMonsterDefense() {
        return battleLogic.getCurrentBattleMonster().getDefence();
    }

    public int getArtifactStrength() {
        return 0;
    }

    public String getMonsterName() {
        return "MONSTER";
    }

    public String getArtifactType() {
        return ArtifactType.ARMOUR.toString();
    }

}
