package com.epam.courses.controller;

import com.epam.courses.model.logic.MapLogic;

public class LevelController {
    private MapLogic mapLogic = MapLogic.getInstance();

    public LevelController() {
    }

    public int getLevel() {
        return mapLogic.getCurrentMapLevel();
    }

    public int getNumberOfMonsters() {
        return mapLogic.getNumberOfMonsters();
    }

    public int[][] getCurrentMapData() {
        return mapLogic.getCurrentMapData();
    }
}
