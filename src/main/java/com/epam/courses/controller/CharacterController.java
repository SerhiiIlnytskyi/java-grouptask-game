package com.epam.courses.controller;

import com.epam.courses.model.logic.CharacterLogic;
import com.epam.courses.model.enums.Direction;

public class CharacterController {

    private static CharacterLogic characterLogic = CharacterLogic.getInstance();

    public CharacterController() {
    }

    public String getName() {
        return characterLogic.getCharacterName();
    }

    public void moveCharacter(Direction direction) {
        characterLogic.moveCharacter(direction);
    }

    public int getCharacterCurrentHealth() {
        return characterLogic.getCharacterHealth();
    }

    public int getCharacterMaximumHealth() {
        return characterLogic.getCharacterHealth();
    }

    public int getCharacterStrength() {
        return characterLogic.getCharacterStrength();
    }

    public int getCharacterDefence() {
        return characterLogic.getCharacterDefence();
    }

    public int getCharacterCurrentXAxis() {
        return characterLogic.getCharacterCurrentXAxis();
    }

    public int getCharacterCurrentYAxis() {
        return characterLogic.getCharacterCurrentYAxis();
    }


    public String getMessageIfAWall() {
        return "WALL!!";
    }

    public String getMessageIfHeroTakeArtifact() {
        return "HERO WIN AND TAKE AN ARTIFACT";
    }

    public String getMessageIfMonsterDied() {
        return "YOU WIN YOU KILL A MONSTER";
    }

    public String getMessageIfBattle() {
        return "YOU ARE FIGHTING A MONSTER";
    }

    public int getCharacterMaxHealth() {
        return characterLogic.getCharacterCurrentHealth();
    }
}


