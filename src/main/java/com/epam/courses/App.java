package com.epam.courses;

import com.epam.courses.view.Game;

public class App {

  public static void main(String[] args) {
    new Game().start();
  }
}
